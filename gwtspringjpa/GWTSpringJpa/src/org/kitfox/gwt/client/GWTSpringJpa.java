package org.kitfox.gwt.client;

import org.kitfox.gwt.shared.factory.EmployeeFactory;
import org.kitfox.gwt.shared.proxy.EmployeeProxy;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GWTSpringJpa implements EntryPoint {
    /**
     * The message displayed to the user when the server cannot be reached or returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while " + "attempting to contact the server. Please check your network "
            + "connection and try again. The error is : ";

    /**
     * Create a remote service proxy to talk to the server-side Employee service.
     */
    private EmployeeFactory employeeFactory;

    /**
     * This is the entry point method.
     */
    @Override
    public void onModuleLoad() {
        final EventBus eventBus = new SimpleEventBus();
        employeeFactory = GWT.create(EmployeeFactory.class);
        employeeFactory.initialize(eventBus);

        final Button saveOrUpdateButton = new Button("SaveOrUpdate");
        final Button retrieveButton = new Button("Retrieve");
        final TextBox employeeInfoField = new TextBox();
        employeeInfoField.setText("1234 nom prenom job 0");
        final TextBox employeeIdField = new TextBox();
        final Label errorLabel = new Label();

        // We can add style names to widgets
        saveOrUpdateButton.addStyleName("sendButton");
        retrieveButton.addStyleName("sendButton");

        // Add the nameField and sendButton to the RootPanel
        // Use RootPanel.get() to get the entire body element
        RootPanel.get("employeeInfoFieldContainer").add(employeeInfoField);
        RootPanel.get("updateEmployeeButtonContainer").add(saveOrUpdateButton);
        RootPanel.get("employeeIdFieldContainer").add(employeeIdField);
        RootPanel.get("retrieveEmployeeButtonContainer").add(retrieveButton);
        RootPanel.get("errorLabelContainer").add(errorLabel);

        // Focus the cursor on the name field when the app loads
        employeeInfoField.setFocus(true);
        employeeInfoField.selectAll();

        // Create the popup dialog box
        final DialogBox dialogBox = new DialogBox();
        dialogBox.setText("Remote Procedure Call");
        dialogBox.setAnimationEnabled(true);
        final Button closeButton = new Button("Close");
        // We can set the id of a widget by accessing its Element
        closeButton.getElement().setId("closeButton");
        final Label textToServerLabel = new Label();
        final HTML serverResponseLabel = new HTML();
        VerticalPanel dialogVPanel = new VerticalPanel();
        dialogVPanel.addStyleName("dialogVPanel");
        dialogVPanel.add(new HTML("<b>Sending request to the server:</b>"));
        dialogVPanel.add(textToServerLabel);
        dialogVPanel.add(new HTML("<b>Server replies:</b>"));
        dialogVPanel.add(serverResponseLabel);
        dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
        dialogVPanel.add(closeButton);
        dialogBox.setWidget(dialogVPanel);

        // Add a handler to close the DialogBox
        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dialogBox.hide();
                saveOrUpdateButton.setEnabled(true);
                saveOrUpdateButton.setFocus(true);
                retrieveButton.setEnabled(true);
            }
        });

        // Create a handler for the saveOrUpdateButton and employeeInfoField
        class SaveOrUpdateEmployeeHandler implements ClickHandler, KeyUpHandler {
            /**
             * Fired when the user clicks on the saveOrUpdateButton.
             */
            @Override
            public void onClick(ClickEvent event) {
                sendEmployeeInfoToServer();
            }

            /**
             * Fired when the user types in the employeeInfoField.
             */
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    sendEmployeeInfoToServer();
                }
            }

            /**
             * Send the employee info from the employeeInfoField to the server and wait for a response.
             */

            private void sendEmployeeInfoToServer() {
                // First, we validate the input.
                errorLabel.setText("");
                String textToServer = employeeInfoField.getText();

                // Then, we send the input to the server.
                saveOrUpdateButton.setEnabled(false);
                textToServerLabel.setText(textToServer);
                serverResponseLabel.setText("");

                String[] employeeInfo = textToServer.split(" ");

                long employeeId = Long.parseLong(employeeInfo[0]);
                String employeeName = employeeInfo[1];
                String employeeSurname = employeeInfo[2];
                String employeeJobTitle = employeeInfo[3];
                long sup = Long.parseLong(employeeInfo[4]);

                try {
                    employeeFactory.employeeRequest().saveOrUpdateEmployee(employeeId, employeeName, employeeSurname, employeeJobTitle, sup)
                            .fire(new Receiver<Void>() {
                                @Override
                                public void onFailure(ServerFailure failure) {
                                    // Show the RPC error message to the user
                                    dialogBox.setText("Remote Procedure Call - Failure");
                                    serverResponseLabel.addStyleName("serverResponseLabelError");
                                    serverResponseLabel.setHTML(SERVER_ERROR + failure.getStackTraceString());
                                    dialogBox.center();
                                    closeButton.setFocus(true);
                                }

                                @Override
                                public void onSuccess(Void noAnswer) {
                                    dialogBox.setText("Remote Procedure Call");
                                    serverResponseLabel.removeStyleName("serverResponseLabelError");
                                    serverResponseLabel.setHTML("OK");
                                    dialogBox.center();
                                    closeButton.setFocus(true);
                                }
                            });
                } catch(Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        // Create a handler for the retrieveButton and employeeIdField
        class RetrieveEmployeeHandler implements ClickHandler, KeyUpHandler {
            /**
             * Fired when the user clicks on the retrieveButton.
             */
            @Override
            public void onClick(ClickEvent event) {
                sendEmployeeIdToServer();
            }

            /**
             * Fired when the user types in the employeeIdField.
             */
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    sendEmployeeIdToServer();
                }
            }

            /**
             * Send the id from the employeeIdField to the server and wait for a response.
             */
            private void sendEmployeeIdToServer() {
                // First, we validate the input.
                errorLabel.setText("");
                String textToServer = employeeIdField.getText();

                // Then, we send the input to the server.
                retrieveButton.setEnabled(false);
                textToServerLabel.setText(textToServer);
                serverResponseLabel.setText("");

                employeeFactory.employeeRequest().findEmployee(Long.parseLong(textToServer)).fire(new Receiver<EmployeeProxy>() {

                    @Override
                    public void onFailure(ServerFailure failure) {
                        // Show the RPC error message to the user
                        dialogBox.setText("Remote Procedure Call - Failure");
                        serverResponseLabel.addStyleName("serverResponseLabelError");
                        serverResponseLabel.setHTML(SERVER_ERROR + failure.getStackTraceString());
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }

                    @Override
                    public void onSuccess(EmployeeProxy employeeProxy) {
                        dialogBox.setText("Remote Procedure Call");
                        serverResponseLabel.removeStyleName("serverResponseLabelError");
                        if(employeeProxy != null) {
                            EmployeeProxy sup = employeeProxy.getSuperieur();
                            String superieur = "BOSS";
                            if(sup != null) {
                                superieur = sup.getEmployeeName();
                            }
                            serverResponseLabel.setHTML("Employee Information Id : " + employeeProxy.getEmployeeId() + " Name : "
                                    + employeeProxy.getEmployeeName() + " Surname : " + employeeProxy.getEmployeeSurname() + " Job Title : "
                                    + employeeProxy.getJob() + " Superieur : " + superieur);
                                
                        } else {
                            serverResponseLabel.setHTML("No employee with the specified id found");
                        }
                        dialogBox.center();
                        closeButton.setFocus(true);
                    }
                });
            }
        }

        // Add a handler to send the employee info to the server
        SaveOrUpdateEmployeeHandler saveOrUpdateEmployeehandler = new SaveOrUpdateEmployeeHandler();
        saveOrUpdateButton.addClickHandler(saveOrUpdateEmployeehandler);
        employeeInfoField.addKeyUpHandler(saveOrUpdateEmployeehandler);

        // Add a handler to send the employee id to the server
        RetrieveEmployeeHandler retrieveEmployeehandler = new RetrieveEmployeeHandler();
        retrieveButton.addClickHandler(retrieveEmployeehandler);
        employeeIdField.addKeyUpHandler(retrieveEmployeehandler);
    }
}
