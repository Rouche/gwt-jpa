package org.kitfox.gwt.server.dao;

import javax.persistence.EntityManagerFactory;

import org.kitfox.gwt.server.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("employeeDAO")
public class EmployeeDAO extends AbstractJpaDAO<Employee> {
    
    @Autowired
    EntityManagerFactory entityManagerFactory;
    
    public EmployeeDAO() {
        super(Employee.class);
    }

}
