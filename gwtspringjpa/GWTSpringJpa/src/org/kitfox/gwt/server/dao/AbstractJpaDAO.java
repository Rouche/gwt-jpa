package org.kitfox.gwt.server.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public abstract class AbstractJpaDAO<T extends Serializable> {

    private Class<T> clazz;

    @PersistenceContext
    private EntityManager entityManager;

    public AbstractJpaDAO(final Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    public T getById(final Long id) {
        return this.entityManager.find(this.clazz, id);
    }

    public List<T> getAll() {
        TypedQuery<T> query = this.entityManager.createQuery("from " + this.clazz.getName(), clazz);
        return query.getResultList();
    }

    public void create(final T entity) {
        this.entityManager.persist(entity);
    }

    public void update(final T entity) {
        this.entityManager.merge(entity);
    }

    public void delete(final T entity) {
        this.entityManager.remove(entity);
    }

    public void deleteById(final Long entityId) {
        final T entity = this.getById(entityId);

        this.delete(entity);
    }
}
