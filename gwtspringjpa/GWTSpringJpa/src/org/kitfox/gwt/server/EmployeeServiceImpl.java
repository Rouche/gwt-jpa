package org.kitfox.gwt.server;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.kitfox.gwt.server.dao.EmployeeDAO;
import org.kitfox.gwt.server.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("employeeService")
public class EmployeeServiceImpl {

    @Autowired
    private EmployeeDAO employeeDAO;

    @PostConstruct
    public void init() throws Exception {
    }

    @PreDestroy
    public void destroy() {
    }

    public Employee findEmployee(long employeeId) {

        return employeeDAO.getById(employeeId);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveEmployee(long employeeId, String name, String surname, String jobDescription) throws Exception {

        Employee employeeDTO = employeeDAO.getById(employeeId);

        if(employeeDTO == null) {
            employeeDTO = new Employee(employeeId, name, surname, jobDescription);
            employeeDAO.create(employeeDTO);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void updateEmployee(long employeeId, String name, String surname, String jobDescription) throws Exception {

        Employee employeeDTO = employeeDAO.getById(employeeId);

        if(employeeDTO != null) {
            employeeDTO.setEmployeeName(name);
            employeeDTO.setEmployeeSurname(surname);
            employeeDTO.setJob(jobDescription);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteEmployee(long employeeId) throws Exception {

        Employee employeeDTO = employeeDAO.getById(employeeId);

        if(employeeDTO != null)
            employeeDAO.delete(employeeDTO);

    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveOrUpdateEmployee(long employeeId, String name, String surname, String jobDescription, long sup) throws Exception {

        Employee employeeDTO = new Employee(employeeId, name, surname, jobDescription);
        
        if(sup > 0) {
            Employee superieur = employeeDAO.getById(sup);
            employeeDTO.setSuperieur(superieur);
        }

        employeeDAO.update(employeeDTO);

    }

}
