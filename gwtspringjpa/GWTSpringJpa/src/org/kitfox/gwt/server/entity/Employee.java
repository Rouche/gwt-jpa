package org.kitfox.gwt.server.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "EMPLOYEE")
public class Employee implements Serializable {

    private static final long serialVersionUID = 7440297955003302414L;

    @Version
    private Long version;

    @Id
    @Column(name = "employee_id")
    private long employeeId;

    @Column(name = "employee_name", nullable = false, length = 30)
    private String employeeName;

    @Column(name = "employee_surname", nullable = false, length = 30)
    private String employeeSurname;

    @Column(name = "job", length = 50)
    private String job;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "superieur_employee_id")
    private Employee superieur;    

    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    private List<Insurances> insurances;

    public Employee() {
    }

    public Employee(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee(long employeeId, String employeeName, String employeeSurname, String job) {
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeSurname = employeeSurname;
        this.job = job;
    }

    public long getId() {
        return employeeId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Employee getSuperieur() {
        return superieur;
    }

    public void setSuperieur(Employee superieur) {
        this.superieur = superieur;
    }

    public List<Insurances> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<Insurances> insurances) {
        this.insurances = insurances;
    }
}
