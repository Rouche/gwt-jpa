package org.kitfox.gwt.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "INSURANCE")
public class Insurances implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3528115923419000129L;

    @Version
    private Long version;

    @Id
    @Column(name = "insurance_id")
    private long insuranceId;

    @Column(name = "insurance_name", nullable = false, length = 30)
    private String insuranceName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id", nullable = false, insertable = false, updatable = false)
    private Employee employee;    

    public Insurances() {
    }

    public long getId() {
        return insuranceId;
    }

    public Insurances(int insuranceId) {
        this.insuranceId = insuranceId;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public long getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(long insuranceId) {
        this.insuranceId = insuranceId;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

}
