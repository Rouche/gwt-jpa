package org.kitfox.gwt.shared.proxy;

import java.util.List;

import org.kitfox.gwt.server.entity.Employee;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyFor(Employee.class)
public interface EmployeeProxy extends ValueProxy {

    public long getEmployeeId();
    public void setEmployeeId(long employeeId);

    public String getEmployeeName();
    public void setEmployeeName(String employeeName);

    public String getEmployeeSurname();
    public void setEmployeeSurname(String employeeSurname);

    public String getJob();
    public void setJob(String job);

    public EmployeeProxy getSuperieur();
    public void setSuperieur(EmployeeProxy superieur);

    public List<InsurancesProxy> getInsurances();
    public void setInsurances(List<InsurancesProxy> insurances);
}
