package org.kitfox.gwt.shared.proxy;

import org.kitfox.gwt.server.entity.Insurances;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyFor(Insurances.class)
public interface InsurancesProxy extends ValueProxy {


    public long getInsuranceId();
    public void setInsuranceId(long insuranceId);

    public String getInsuranceName();
    public void setInsuranceName(String insuranceName);

    public EmployeeProxy getEmployee();
    public void setEmployee(EmployeeProxy employee);
}
