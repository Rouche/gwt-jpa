package org.kitfox.gwt.shared.factory;

import org.kitfox.gwt.shared.services.EmployeeRequest;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

public interface EmployeeFactory extends RequestFactory {

    public EmployeeRequest employeeRequest();

}
