package org.kitfox.gwt.shared.services;

import org.kitfox.gwt.shared.proxy.EmployeeProxy;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface EmployeeServiceAsync {

    void deleteEmployee(long employeeId, AsyncCallback<Void> callback);

    void findEmployee(long employeeId, AsyncCallback<EmployeeProxy> callback);

    void saveEmployee(long employeeId, String name, String surname, String jobDescription, AsyncCallback<Void> callback);

    void saveOrUpdateEmployee(long employeeId, String name, String surname, String jobDescription, long sup, AsyncCallback<Void> callback);

    void updateEmployee(long employeeId, String name, String surname, String jobDescription, AsyncCallback<Void> callback);

}
