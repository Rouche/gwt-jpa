package org.kitfox.gwt.shared.services;

import org.kitfox.gwt.server.EmployeeServiceImpl;
import org.kitfox.gwt.server.locator.SpringServiceLocator;
import org.kitfox.gwt.shared.proxy.EmployeeProxy;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;

//@RemoteServiceRelativePath("springGwtServices/employeeService")
@Service(value=EmployeeServiceImpl.class, locator=SpringServiceLocator.class)
public interface EmployeeRequest extends RequestContext {

    public Request<EmployeeProxy> findEmployee(long employeeId);

    public Request<Void> saveEmployee(long employeeId, String name, String surname, String jobDescription) throws Exception;

    public Request<Void> updateEmployee(long employeeId, String name, String surname, String jobDescription) throws Exception;

    public Request<Void> saveOrUpdateEmployee(long employeeId, String name, String surname, String jobDescription, long sup) throws Exception;

    public Request<Void> deleteEmployee(long employeeId) throws Exception;

}
